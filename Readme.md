# RPG Characters

## Introduction
This project's purpose is to learn object-oriented programming fundamentals in Java.

The project is an RPG game that have different type of characters, weapons and armor, and certain characters can only
equip certain weapons and armor.

## Packages
### Character
The Character package contains classes that handles the characters, their properties and attributes.
### Item
The Item package contains classes that handles the items, their properties and attributes.
### Exception
The Exception package contains exception classes for InvalidArmorException and InvalidWeaponException.

## Tests
The project also contain tests for many of the functions.