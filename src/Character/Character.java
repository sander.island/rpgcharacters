package Character;

import Exception.InvalidArmorException;
import Exception.InvalidWeaponException;
import Item.Armor;
import Item.Item;
import Item.ItemSlot;
import Item.Weapon;

import java.util.HashMap;

/**
 * Character class
 * @author Sander Island
 */
public abstract class Character {
    protected String name;
    protected int level;
    protected int baseStrength;
    protected int baseDexterity;
    protected int baseIntelligence;
    protected int totalStrength;
    protected int totalDexterity;
    protected int totalIntelligence;
    protected int totalPrimaryAttribute;
    protected double characterDps;
    protected HashMap<ItemSlot, Item> equipment = new HashMap<>();
    protected double dps;

    /**
     * Constructor for the Character class
     * @param name
     */
    public Character(String name) {
        this.name = name;
        this.level = 1;
    }

    /**
     * Level up the character
     */
    abstract void levelUp();

    /**
     * Checks if a character can equip a given item
     */
    abstract boolean canEquipItem(Item item);

    /**
     * Puts the character stats in a StringBuilder.
     * @return stats
     */
    public StringBuilder getCharacterStats() {
        StringBuilder stats = new StringBuilder();
        stats.append(this.name + " ");
        stats.append("level " + this.level + "\n");
        stats.append("Strength: " + this.totalStrength + "\n");
        stats.append("Dexterity: " + this.totalDexterity + "\n");
        stats.append("Intelligence: " + this.totalIntelligence + "\n");
        stats.append("DPS: " + String.format("%.02f", this.characterDps));
        return stats;
    }

    /**
     * Equip an item
     * @param item
     * @param slot
     */
    public void equip(Item item, ItemSlot slot) {
        if (item.getClass() == Armor.class) { // Checks if the item is an armor
            try {
                this.equipArmor(item, slot);
                this.calculateStats();
            } catch (InvalidArmorException error) {
                System.out.println("Could not equip " + item.getName());
            }
        } else if (item.getClass() == Weapon.class) { // Checks if the item is a weapon
            try {
                this.equipWeapon(item, slot);
                this.calculateStats();
            } catch (InvalidWeaponException error) {
                System.out.println("Could not equip weapon " + item.getName());
            }
        }
    }

    /**
     * Checks if an armor can be equipped, and then equips the armor
     * If not - throws InvalidArmorException
     * @param armor
     * @param itemSlot
     * @throws InvalidArmorException
     */
    public void equipArmor(Item armor, ItemSlot itemSlot) throws InvalidArmorException {
        if (itemSlot == ItemSlot.HEAD || armor.getItemSlot() == ItemSlot.BODY || armor.getItemSlot() == ItemSlot.LEGS) { // Checks if the item is an armor
            if (this.canEquipItem(armor) && this.level >= armor.getRequiredLevel()) { // Checks if the character can equip the armor and is the required level
                equipment.put(itemSlot, armor); // Equips the armor
            } else {
                throw new InvalidArmorException("Character cannot equip this armor or character level is to low");
            }
        } else {
            throw new InvalidArmorException("Can't equip " + armor.getName() + " to slot " + itemSlot);
        }

    }

    /**
     * Checks if a weapon can be equipped, and then equips the weapon
     * If not - throws InvalidWeaponException
     * @param weapon
     * @param itemSlot
     * @throws InvalidWeaponException
     */
    public void equipWeapon(Item weapon, ItemSlot itemSlot) throws InvalidWeaponException {
        if (itemSlot == ItemSlot.WEAPON) { // Checks if the item is a weapon
            if (this.canEquipItem(weapon) && this.level >= weapon.getRequiredLevel()) { // Checks if the character can equip the weapon and is the required level
                equipment.put(itemSlot, weapon); // Equips the weapon
            } else {
                throw new InvalidWeaponException("Character can't equip item or character level is too low");
            }
        } else {
            throw new InvalidWeaponException("Can't equip weapon " + weapon.getName() + " to slot " + itemSlot);
        }
    }

    /**
     * @return DPS
     */
    public double getDps() {
        return dps;
    }

    /**
     * Calculate a character's stats
     */
    public void calculateStats() {
        this.totalStrength = this.baseStrength;
        this.totalDexterity = this.baseDexterity;
        this.totalIntelligence = this.baseIntelligence;
        this.totalPrimaryAttribute = this.totalStrength + this.totalDexterity + this.totalIntelligence;
        double weaponDps = getDps(); // A weapon's DPS
        if (this.equipment.isEmpty()) { // Checks if character has equipped any equipment
            this.characterDps = 1 + this.totalPrimaryAttribute / 100f;
        } else {
            for (ItemSlot key : this.equipment.keySet()) { // Iterate through the equipped equipment
                if (key == ItemSlot.WEAPON) { // Equipment is a weapon
                    weaponDps = ((Weapon) this.equipment.get(key)).getDps();
                } else { // Equipment is an armor
                    this.totalStrength = ((Armor) this.equipment.get(key)).getStrength();
                    this.totalDexterity = ((Armor) this.equipment.get(key)).getDexterity();
                    this.totalIntelligence = ((Armor) this.equipment.get(key)).getIntelligence();
                    this.totalPrimaryAttribute = this.totalStrength + this.totalDexterity + this.totalIntelligence;
                }
            }
            if (weaponDps > 0) {
                this.characterDps = weaponDps * (1 + this.totalPrimaryAttribute / 100f);
            } else {
                this.characterDps = (1 + this.totalPrimaryAttribute / 100f);
            }
        }
    }
}