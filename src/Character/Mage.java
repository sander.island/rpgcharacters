package Character;

import Item.Item;
import Item.ItemType;

/**
 * Mage class
 */
public class Mage extends Character {

    /**
     * Constructor for the Mage class
     * @param name
     */
    public Mage(String name) {
        super(name);
        this.baseStrength = 1;
        this.baseDexterity = 1;
        this.baseIntelligence = 8;
        this.totalStrength = 1;
        this.totalDexterity = 1;
        this.totalIntelligence = 8;
        this.calculateStats();
    }

    /**
     * Level up the mage
     */
    public void levelUp() {
        this.level += 1;
        this.baseStrength += 1;
        this.baseDexterity += 1;
        this.baseIntelligence += 5;
        calculateStats();
        System.out.println(name + " leveled up to level " + level + "!");
    }

    /**
     * Checks if the mage can equip a given item
     * Mage can equip CLOTH, WAND and STAFF
     * @param item
     */
    boolean canEquipItem(Item item) {
        if (item.getName() == ItemType.CLOTH || item.getName() == ItemType.WAND || item.getName() == ItemType.STAFF) {
            return true;
        } else return false;
    }
}