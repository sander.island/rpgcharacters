package Character;

import Item.Item;
import Item.ItemType;

/**
 * Ranger class
 */
public class Ranger extends Character {

    /**
     * Constructor for the Ranger class
     * @param name
     */
    public Ranger(String name) {
        super(name);
        this.baseStrength = 1;
        this.baseDexterity = 7;
        this.baseIntelligence = 1;
        this.totalStrength = 1;
        this.totalDexterity = 7;
        this.totalIntelligence = 1;
        this.calculateStats();
    }

    /**
     * Level up the ranger
     */
    void levelUp() {
        this.level += 1;
        this.baseStrength += 1;
        this.baseDexterity += 5;
        this.baseIntelligence += 1;
        calculateStats();
        System.out.println(name + " leveled up to level " + level + "!");
    }

    /**
     * Checks if the ranger can equip a given item
     * Ranger can equip LEATHER, MAIL and BOW
     * @param item
     */
    boolean canEquipItem(Item item) {
        if (item.getName() == ItemType.LEATHER || item.getName() == ItemType.MAIL || item.getName() == ItemType.BOW) {
            return true;
        } else return false;
    }
}
