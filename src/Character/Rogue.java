package Character;

import Item.Item;
import Item.ItemType;

/**
 * Rogue class
 */
public class Rogue extends Character {

    /**
     * Constructor for the Rogue class
     * @param name
     */
    public Rogue(String name) {
        super(name);
        this.baseStrength = 2;
        this.baseDexterity = 6;
        this.baseIntelligence = 1;
        this.totalStrength = 2;
        this.totalDexterity = 6;
        this.totalIntelligence = 1;
        this.calculateStats();
    }

    /**
     * Level up the rogue
     */
    void levelUp() {
        this.level += 1;
        this.baseStrength += 1;
        this.baseDexterity += 4;
        this.baseIntelligence += 1;
        calculateStats();
        System.out.println(name + " leveled up to level " + level + "!");
    }

    /**
     * Checks if the rogue can equip a given item
     * Rogue can equip MAIL, LEATHER, DAGGER and SWORD
     * @param item
     */
    boolean canEquipItem(Item item) {
        if (item.getName() == ItemType.MAIL || item.getName() == ItemType.LEATHER || item.getName() == ItemType.DAGGER || item.getName() == ItemType.SWORD) {
            return true;
        } else return false;
    }
}
