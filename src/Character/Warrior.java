package Character;

import Item.Item;
import Item.ItemType;

/**
 * Warrior class
 */
public class Warrior extends Character {

    /**
     * Constructor for the Warrior class
     * @param name
     */
    public Warrior(String name) {
        super(name);
        this.baseStrength = 5;
        this.baseDexterity = 2;
        this.baseIntelligence = 1;
        this.totalStrength = 5;
        this.totalDexterity = 2;
        this.totalIntelligence = 1;
        this.calculateStats();
    }

    /**
     * Level up the warrior
     */
    void levelUp() {
        this.level += 1;
        this.baseStrength += 3;
        this.baseDexterity += 2;
        this.baseIntelligence += 1;
        calculateStats();
        System.out.println(name + " leveled up to level " + level + "!");
    }

    /**
     * Checks if the warrior can equip a given item
     * Warrior can equip MAIL, PLATE, AXE, HAMMER and SWORD
     * @param item
     */
    boolean canEquipItem(Item item) {
        if (item.getName() == ItemType.MAIL || item.getName() == ItemType.PLATE || item.getName() == ItemType.AXE || item.getName() == ItemType.HAMMER || item.getName() == ItemType.SWORD) {
            return true;
        } else return false;
    }
}
