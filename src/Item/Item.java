package Item;

/**
 * Item class
 */
public abstract class Item {
    protected final ItemType name;
    protected final int requiredLevel;
    protected ItemSlot itemSlot;

    /**
     * Constructor for the Item class
     * @param name
     * @param requiredLevel
     * @param itemSlot
     */
    public Item(ItemType name, int requiredLevel, ItemSlot itemSlot) {
        this.name = name;
        this.requiredLevel = requiredLevel;
        this.itemSlot = itemSlot;
    }

    /**
     * @return name
     */
    public ItemType getName() {
        return this.name;
    }

    /**
     * @return requiredLevel
     */
    public int getRequiredLevel() {
        return this.requiredLevel;
    }

    /**
     * @return itemSlot
     */
    public ItemSlot getItemSlot() {
        return this.itemSlot;
    }
}
