package Item;

/**
 * Weapon class
 */
public class Weapon extends Item {
    private int damage;
    private int attackPerSecond;

    /**
     * Constructor for the Weapon class
     * @param name
     * @param requiredLevel
     * @param itemSlot
     * @param damage
     * @param attackPerSecond
     */
    public Weapon(ItemType name, int requiredLevel, ItemSlot itemSlot, int damage, int attackPerSecond) {
        super(name, requiredLevel, ItemSlot.WEAPON);
        this.itemSlot = itemSlot;
        this.damage = damage;
        this.attackPerSecond = attackPerSecond;
    }

    /**
     * @return name
     */
    public ItemType getName()
    {
        return this.name;
    }

    /**
     * @return DPS
     */
    public double getDps()
    {
        return this.damage * this.attackPerSecond;
    }
}
