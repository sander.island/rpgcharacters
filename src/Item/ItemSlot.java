package Item;

/**
 * ItemSlot enum, an item can be equipped in the slots HEAD, BODY, LEGS and WEAPON
 */
public enum ItemSlot {
    HEAD,
    BODY,
    LEGS,
    WEAPON
}
