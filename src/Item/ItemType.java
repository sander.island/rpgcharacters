package Item;

/**
 * ItemType enum
 * An item can be of type AXE, BOW, CLOTH, DAGGER, HAMMER, LEATHER, MAIL, PLATE, STAFF SWORD and WAND
 */
public enum ItemType {
    AXE,
    BOW,
    CLOTH,
    DAGGER,
    HAMMER,
    LEATHER,
    MAIL,
    PLATE,
    STAFF,
    SWORD,
    WAND
}
