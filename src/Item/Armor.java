package Item;

/**
 * Armor class
 */
public class Armor extends Item {
    private int strength;
    private int dexterity;
    private int intelligence;

    /**
     * Constructor for the Armor class
     * @param name
     * @param requiredLevel
     * @param itemSlot
     * @param strength
     * @param dexterity
     * @param intelligence
     */
    public Armor(ItemType name, int requiredLevel, ItemSlot itemSlot, int strength, int dexterity, int intelligence) {
        super(name, requiredLevel, itemSlot);
        this.strength = strength;
        this.dexterity = dexterity;
        this.intelligence = intelligence;
    }

    /**
     * @return strength
     */
    public int getStrength() {
        return this.strength;
    }

    /**
     * @return dexterity
     */
    public int getDexterity() {
        return this.dexterity;
    }

    /**
     * @return intelligence
     */
    public int getIntelligence() {
        return this.intelligence;
    }
}
