import Character.Mage;
import Item.ItemSlot;
import Item.ItemType;
import Item.Weapon;

/**
 * Main class, runs the game
 */
public class Main {

    /**
     * Makes a character, level it up, equips an item and prints stats
     * @param args
     */
    public static void main(String[] args) {
        Mage mage1 = new Mage("TestMage");
        System.out.println(mage1.getCharacterStats());
        System.out.println();

        mage1.levelUp();
        System.out.println(mage1.getCharacterStats());

        Weapon wand1 = new Weapon(ItemType.WAND, 1, ItemSlot.WEAPON, 2, 2);
        mage1.equip(wand1, ItemSlot.WEAPON);
        System.out.println(mage1.getCharacterStats());
    }
}
