package Exception;

/**
 * InvalidArmorException class
 */
public class InvalidArmorException extends Exception{
    /**
     * Invalid Armor Exception
     * @param message
     */
    public InvalidArmorException(String message) {
        super(message);
    }
}
