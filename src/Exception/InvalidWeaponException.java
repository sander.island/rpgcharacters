package Exception;

/**
 * InvalidWeaponException class
 */
public class InvalidWeaponException extends Exception{
    /**
     * Invalid Weapon Exception
     * @param message
     */
    public InvalidWeaponException(String message) {
        super(message);
    }
}
