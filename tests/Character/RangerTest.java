package Character;

import Item.ItemSlot;
import Item.ItemType;
import Item.Weapon;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Ranger Test class
 */
class RangerTest {

    /**
     * Tests if the ranger levelUp() method works
     */
    @Test
    void levelUp() {
        //Arrange
        Ranger ranger = new Ranger("TestRanger");
        int expected = 2;

        //Act
        ranger.levelUp();
        int actual = ranger.level;

        //Assert
        assertEquals(actual,expected);
    }

    /**
     * Tests if the ranger canEquipItem() method works
     * Tries to equip a compatible item
     */
    @Test
    void canEquipItem_validInput_shouldReturnTrue() {
        //Arrange
        Ranger ranger = new Ranger("TestRanger");
        Weapon bow = new Weapon(ItemType.BOW, 1, ItemSlot.WEAPON, 2, 2);
        boolean expected = true;

        //Act
        ranger.equip(bow,ItemSlot.WEAPON);
        boolean actual = ranger.canEquipItem(bow);

        //Assert
        assertEquals(actual, expected);
    }

    /**
     * Tests if the ranger canEquipItem() method works
     * Tries to equip an incompatible item
     */
    @Test
    void canEquipItem_invalidInput_shouldReturnFalse() {
        //Arrange
        Ranger ranger = new Ranger("TestRanger");
        Weapon hammer = new Weapon(ItemType.HAMMER, 1, ItemSlot.WEAPON, 2, 2);
        boolean expected = false;

        //Act
        ranger.equip(hammer,ItemSlot.WEAPON);
        boolean actual = ranger.canEquipItem(hammer);

        //Assert
        assertEquals(actual, expected);
    }
}