package Character;

import Item.ItemSlot;
import Item.ItemType;
import Item.Weapon;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Rogue Test class
 */
class RogueTest {

    /**
     * Tests if the rogue levelUp() method works
     */
    @Test
    void levelUp() {
        //Arrange
        Rogue rogue = new Rogue("TestRogue");
        int expected = 2;

        //Act
        rogue.levelUp();
        int actual = rogue.level;

        //Assert
        assertEquals(actual,expected);
    }

    /**
     * Tests if the rogue canEquipItem() method works
     * Tries to equip a compatible item
     */
    @Test
    void canEquipItem_validInput_shouldReturnTrue() {
        //Arrange
        Rogue rogue = new Rogue("TestRogue");
        Weapon dagger = new Weapon(ItemType.DAGGER, 1, ItemSlot.WEAPON, 2, 2);
        boolean expected = true;

        //Act
        rogue.equip(dagger,ItemSlot.WEAPON);
        boolean actual = rogue.canEquipItem(dagger);

        //Assert
        assertEquals(actual, expected);
    }

    /**
     * Tests if the rogue canEquipItem() method works
     * Tries to equip an incompatible item
     */
    @Test
    void canEquipItem_invalidInput_shouldReturnFalse() {
        //Arrange
        Rogue rogue = new Rogue("TestRogue");
        Weapon hammer = new Weapon(ItemType.HAMMER, 1, ItemSlot.WEAPON, 2, 2);
        boolean expected = false;

        //Act
        rogue.equip(hammer,ItemSlot.WEAPON);
        boolean actual = rogue.canEquipItem(hammer);

        //Assert
        assertEquals(actual, expected);
    }
}