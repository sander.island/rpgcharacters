package Character;

import Exception.InvalidArmorException;
import Exception.InvalidWeaponException;
import Item.Armor;
import Item.ItemSlot;
import Item.ItemType;
import Item.Weapon;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

/**
 * Character Test class
 */
class CharacterTest {

    /**
     * Tests if the getCharacterStats() method works
     */
    @Test
    void getCharacterStats() {
        //Arrange
        Mage mage = new Mage("TestMage");
        StringBuilder expected = new StringBuilder();
        expected.append("TestMage" + " ");
        expected.append("level " + 1 + "\n");
        expected.append("Strength: " + 1 + "\n");
        expected.append("Dexterity: " + 1 + "\n");
        expected.append("Intelligence: " + 8 + "\n");
        expected.append("DPS: " + "1,10");

        //Act
        StringBuilder actual = mage.getCharacterStats();

        //Assert
        assertEquals(expected.toString(), actual.toString());
    }

    /**
     * Tests if the equip() method works
     * Tries to equip an incompatible weapon to throw InvalidWeaponException
     */
    @Test
    void equip_throwInvalidWeaponException_shouldPrintError() {
        //Arrange
        String exceptedOutput = "Could not equip weapon " + ItemType.AXE + "\r\n";
        Rogue rogue = new Rogue("TestRogue");
        Weapon axe = new Weapon(ItemType.AXE, 1, ItemSlot.WEAPON, 1, 1);
        ByteArrayOutputStream actualOutput = new ByteArrayOutputStream();
        System.setOut(new PrintStream(actualOutput));

        //Act
        rogue.equip(axe, ItemSlot.WEAPON);

        //Assert
        assertEquals(exceptedOutput, actualOutput.toString());
    }

    /**
     * Tests if the equip() method works
     * Tries to equip an incompatible armor to throw InvalidArmorException
     */
    @Test
    void equip_throwInvalidArmorException_shouldPrintError() {
        //Arrange
        String exceptedOutput = "Could not equip " + ItemType.PLATE + "\r\n";
        Rogue rogue = new Rogue("TestRogue");
        Armor plate = new Armor(ItemType.PLATE, 1, ItemSlot.BODY, 1, 1, 1);
        ByteArrayOutputStream actualOutput = new ByteArrayOutputStream();
        System.setOut(new PrintStream(actualOutput));

        //Act
        rogue.equip(plate, ItemSlot.BODY);

        //Assert
        assertEquals(exceptedOutput, actualOutput.toString());
    }


    /**
     * Tests if the equipArmor() method works
     * Tries to equip an incompatible armor to throw InvalidArmorException
     */
    @Test
    void equipArmor_equipInvalidArmor_shouldThrowInvalidArmorException() {
        //Arrange
        String expectedMsg = "Character cannot equip this armor or character level is to low";
        Rogue rogue = new Rogue("TestRogue");
        Armor plate = new Armor(ItemType.PLATE, 2, ItemSlot.BODY, 1, 1, 1);
        Exception exception = assertThrows(InvalidArmorException.class, () -> rogue.equipArmor(plate, ItemSlot.BODY));

        //Act
        String actualMsg = exception.getMessage();

        //Assert
        assertEquals(expectedMsg, actualMsg);
    }

    /**
     * Tests if the equipWeapon() method works
     * Tries to equip an incompatible weapon to throw InvalidWeaponException
     */
    @Test
    void equipWeapon_equipInvalidWeapon_shouldThrowInvalidWeaponException() {
        //Arrange
        String expectedMsg = "Can't equip weapon " + ItemType.SWORD + " to slot " + ItemSlot.HEAD;
        Mage mage = new Mage("TestMage");
        Weapon sword = new Weapon(ItemType.SWORD, 1, ItemSlot.WEAPON, 1, 1);
        Exception exception = assertThrows(InvalidWeaponException.class, () -> mage.equipWeapon(sword, ItemSlot.HEAD));

        //Act
        String actualMsg = exception.getMessage();

        //Assert
        assertEquals(expectedMsg, actualMsg);
    }


    /**
     * Tests if the calculateStats() method works
     */
    @Test
    void calculateStats() {
        //Arrange
        int expectedStrength = 1;
        int expectedDexterity = 1;
        int expectedIntelligence = 8;
        float expectedCharacterDps = 1.10f;
        Mage mage = new Mage("testMage");

        //Act
        int actualStrength = mage.totalStrength;
        int actualDexterity = mage.totalDexterity;
        int actualIntelligence = mage.totalIntelligence;
        double actualCharacterDps = mage.characterDps;

        //Assert
        assertEquals(expectedStrength, actualStrength);
        assertEquals(expectedDexterity, actualDexterity);
        assertEquals(expectedIntelligence, actualIntelligence);
        assertEquals(expectedCharacterDps, actualCharacterDps);
    }
}