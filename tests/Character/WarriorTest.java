package Character;

import Item.ItemSlot;
import Item.ItemType;
import Item.Weapon;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Warrior Test class
 */
class WarriorTest {

    /**
     * Tests if the warrior levelUp() method works
     */
    @Test
    void levelUp() {
        //Arrange
        Warrior warrior = new Warrior("TestWarrior");
        int expected = 2;

        //Act
        warrior.levelUp();
        int actual = warrior.level;

        //Assert
        assertEquals(actual,expected);
    }

    /**
     * Tests if the warrior canEquipItem() method works
     * Tries to equip a compatible item
     */
    @Test
    void canEquipItem_validInput_shouldReturnTrue() {
        //Arrange
        Warrior warrior = new Warrior("TestWarrior");
        Weapon sword = new Weapon(ItemType.SWORD, 1, ItemSlot.WEAPON, 2, 2);
        boolean expected = true;

        //Act
        warrior.equip(sword,ItemSlot.WEAPON);
        boolean actual = warrior.canEquipItem(sword);

        //Assert
        assertEquals(actual, expected);
    }

    /**
     * Tests if the warrior canEquipItem() method works
     * Tries to equip an incompatible item
     */
    @Test
    void canEquipItem_invalidInput_shouldReturnFalse() {
        //Arrange
        Warrior warrior = new Warrior("TestWarrior");
        Weapon wand = new Weapon(ItemType.WAND, 1, ItemSlot.WEAPON, 2, 2);
        boolean expected = false;

        //Act
        warrior.equip(wand,ItemSlot.WEAPON);
        boolean actual = warrior.canEquipItem(wand);

        //Assert
        assertEquals(actual, expected);
    }
}