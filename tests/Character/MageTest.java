package Character;

import Item.ItemSlot;
import Item.ItemType;
import Item.Weapon;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Mage Test class
 */
class MageTest {

    /**
     * Tests if the mage levelUp() method works
     */
    @Test
    void levelUp() {
        //Arrange
        Mage mage = new Mage("TestMage");
        int expected = 2;

        //Act
        mage.levelUp();
        int actual = mage.level;

        //Assert
        assertEquals(actual,expected);
    }

    /**
     * Tests if the mage canEquipItem() method works
     * Tries to equip a compatible item
     */
    @Test
    void canEquipItem_validInput_shouldReturnTrue() {
        //Arrange
        Mage mage = new Mage("TestMage");
        Weapon wand = new Weapon(ItemType.WAND, 1, ItemSlot.WEAPON, 2, 2);
        boolean expected = true;

        //Act
        mage.equip(wand,ItemSlot.WEAPON);
        boolean actual = mage.canEquipItem(wand);

        //Assert
        assertEquals(actual, expected);
    }

    /**
     * Tests if the mage canEquipItem() method works
     * Tries to equip an incompatible item
     */
    @Test
    void canEquipItem_invalidInput_shouldReturnFalse() {
        //Arrange
        Mage mage = new Mage("TestMage");
        Weapon hammer = new Weapon(ItemType.HAMMER, 1, ItemSlot.WEAPON, 2, 2);
        boolean expected = false;

        //Act
        mage.equip(hammer,ItemSlot.WEAPON);
        boolean actual = mage.canEquipItem(hammer);

        //Assert
        assertEquals(actual, expected);
    }
}