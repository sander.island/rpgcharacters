package Item;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Item Test class
 */
class ItemTest {

    /**
     * Tests if the item getName() method works
     */
    @Test
    void getName() {
        //Arrange
        Weapon bow = new Weapon(ItemType.BOW, 1, ItemSlot.WEAPON, 2, 2);
        ItemType expected = ItemType.BOW;

        //Act
        ItemType actual = bow.getName();

        //Assert
        assertEquals(expected, actual);

    }

    /**
     * Tests if the item getRequiredLevel() method works
     */
    @Test
    void getRequiredLevel() {
        //Arrange
        Weapon sword = new Weapon(ItemType.SWORD, 1, ItemSlot.WEAPON, 2, 2);
        int expected = 1;

        //Act
        int actual = sword.getRequiredLevel();

        //Assert
        assertEquals(expected, actual);

    }

    /**
     * Tests if the item getItemSlot() method works
     */
    @Test
    void getItemSlot() {
        //Arrange
        Weapon staff = new Weapon(ItemType.STAFF, 1,ItemSlot.WEAPON, 2, 2);
        ItemSlot expected = ItemSlot.WEAPON;

        //Act
        ItemSlot actual = staff.getItemSlot();

        //Assert
        assertEquals(expected, actual);
    }
}