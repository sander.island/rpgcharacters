package Item;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Weapon Test class
 */
class WeaponTest {

    /**
     * Tests if the weapon getDps() method works
     */
    @Test
    void getDps() {
        //Arrange
        double expected = 4.0;
        Weapon sword = new Weapon(ItemType.SWORD, 1, ItemSlot.WEAPON, 2,2);

        //Act
        double actual = sword.getDps();

        //Assert
        assertEquals(expected, actual);
    }
}