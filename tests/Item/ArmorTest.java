package Item;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Armor Test class
 */
class ArmorTest {
    //Arrange
    Armor mail = new Armor(ItemType.MAIL, 1, ItemSlot.BODY, 1, 3, 2);

    /**
     * Tests if the armor getStrength() method works
     */
    @Test
    void getStrength() {
        //Arrange
        int expected = 1;

        //Act
        int actual = mail.getStrength();

        //Assert
        assertEquals(expected, actual);
    }

    /**
     * Tests if the armor getDexterity() method works
     */
    @Test
    void getDexterity() {
        //Arrange
        int expected = 3;

        //Act
        int actual = mail.getDexterity();

        //Assert
        assertEquals(expected, actual);
    }

    /**
     * Tests if the armor getIntelligence() method works
     */
    @Test
    void getIntelligence() {
        //Arrange
        int expected = 2;

        //Act
        int actual = mail.getIntelligence();

        //Assert
        assertEquals(expected, actual);
    }
}